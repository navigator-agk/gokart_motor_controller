void updateDI(void)
{
  int buttonValue = 0;
  static unsigned int counterOn = 0;
  static unsigned int counterOff = 0;
  
  buttonValue = digitalRead(DISABLE_PIN);
  
  if(buttonValue == HIGH) // "Disable" Switch
  {
    counterOff = 0;
    counterOn++;
    if (counterOn > ON_COUNT && counterOn <= (ON_COUNT + 1) && !disablePressed)
    {
      disablePressed = true;
    }
  }
  else if (disablePressed)
  {
    counterOff++;
    if (counterOff > OFF_COUNT && counterOff <= (OFF_COUNT + 1))
    {
      counterOn = 0;
      disablePressed = false;
    }
  }
 
}

//======================================================================================
//===============================Output Helper Functions================================
//======================================================================================
void setThrottle(double throttle)
{
  byte writeValue = 0;

  writeValue = map((int)throttle, 0, 100, 42, 153);
  analogWrite(THROTTLE_PIN, writeValue);
}


void setBrake(byte engageBrake)
{
  if (engageBrake){ digitalWrite(BRAKE_PIN, HIGH); brakeEngaged = true;}
  else{ digitalWrite(BRAKE_PIN, LOW); brakeEngaged = false; }
}


void setReverse(byte reverse)
{
 if(reverse){ digitalWrite(REVERSE_PIN, HIGH); }
 else { digitalWrite(REVERSE_PIN, LOW); } 
}


void setGear(byte gear)
{
  // PN - These need to be adjusted to provide the correct behavior
  switch (gear)
  {
    case 1:
      // Both relays should be open
      digitalWrite(GEAR_PIN_A, LOW);
      digitalWrite(GEAR_PIN_B, HIGH);
      currentGear = 1;
      break;
      
    case 2:
      digitalWrite(GEAR_PIN_A, LOW);
      digitalWrite(GEAR_PIN_B, LOW);
      currentGear = 2;
      break;
      
    case 3:
      digitalWrite(GEAR_PIN_A, HIGH);
      digitalWrite(GEAR_PIN_B, LOW);
      currentGear = 3;
      break;
      
    default:
      // Leave it in 1st gear if some other value is provided
      digitalWrite(GEAR_PIN_A, LOW);
      digitalWrite(GEAR_PIN_B, HIGH);
      currentGear = 1;
      break;
  }
}
