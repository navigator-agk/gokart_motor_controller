void stateLoop(void)
{
  /* This is where all important actions are taken.
   *
   */

  updateState();

  // ============================= PERFORM PROPER ACTIONS BASED ON STATE ===========================
  switch (State)
  {
    case ACTIVE_STATE:
      // Disable the brake before trying to move
      if (brakeEngaged){ setBrake(false); }
      
      // Perform the "gear" switch
      if(currentGear != desiredGear){ setGear(desiredGear); } // IO

      // Smooth the transition between the desired throttle and active throttle
      throttlePercentEffort = 0.9*throttlePercentEffort + 0.1*(double)throttlePercentEffortDriver;

      setReverse(reverseRequested);
      setThrottle(throttlePercentEffort);
      break;

    case E_BRAKE_STATE:
      // Throttle should be set to 0 effort and commands should be ignored
      // electronic brake is activated
      setReverse(0);
      setThrottle(0);
      setBrake(true);
      break;

    case INACTIVE_STATE:
      // Defaults all controls to "zero" values. Does not heed serial commands
      setReverse(0);  // Set to forward
      setThrottle(0); // 0 throttle
      setGear(1);     // 1st Gear
      if (brakeEngaged){ setBrake(false); }
      break;
      
  } // End of switch case

} // End of function


//======================================================================================
//==============================State Transition Function===============================
//======================================================================================

void updateState(void)
{
  // Update desired state
  if(disablePressed || serialTimedOut){ desiredState = INACTIVE_STATE; }
  else if (brakeRequested){ desiredState = E_BRAKE_STATE; }
  else{ desiredState = ACTIVE_STATE; }

  State = desiredState;

} // End of function
