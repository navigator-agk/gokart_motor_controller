/*  Written by: Patrick Neal
 *  Email: neap@ufl.edu
 *  Last Updated: 9/19/2022
 *  
 *  Questions:
 *    - What kind of feedback should be sent to the ROS2 driver?
 *    
 *  
 *  Notes:
 *    * Allow the arduino some time after opening a serial connection. (like 2 seconds) 
 *    * Make sure to clear the TX buffer on the host computer when connecting to this sketch
 *
 *    
 */

// ---- Pin Definitions ----
// PN - Adjust the pin mapping during testing
#define DISABLE_PIN 18
#define GEAR_PIN_A 8
#define GEAR_PIN_B 9
#define BRAKE_PIN 10
#define REVERSE_PIN 12
#define CRUISE_PIN 11
#define THROTTLE_PIN 13

// ---- DIO Debouncing ----
#define ON_COUNT 12
#define OFF_COUNT 30

#define ACTIVE_STATE      1
#define E_BRAKE_STATE     2 // Electronic brake
#define INACTIVE_STATE    3

// ---- Serial Communication Parameters ----
#define BAUDRATE        115200
#define TX_PACKET_SIZE  6
#define RX_PACKET_SIZE  7
#define CRC_DIVIDER     256
#define HEARTBEAT_TIMEOUT 3000 // Time in milliseconds that must pass before heart beat timeout is triggered

// ---Loop Timers----
#define STATE_TIMER 10   // Time in millseconds between each call of the state loop (~100Hz)
#define SEND_TIMER 100   // Time in millseconds between each call of sendMessage (~10Hz)

//======================================================================================
//===============================Global Variables=======================================
//======================================================================================
int State = INACTIVE_STATE;           // start in the inactive State, ignition is off
int desiredState = INACTIVE_STATE;    // stores the desired state based on state transition logic

// ---- Received From ROS2 Driver ----
byte brakeRequested = 0;                  // 0 if no brake requested from ROS2 driver, 1 if requested from ROS2 driver
byte reverseRequested = 0;                // 0 if no reverse requested from ROS2 driver, 1 if requested from ROS2 driver
byte cruiseRequested = 0;                 // 0 if no cruise control requested from ROS2 driver, 1 if requested from ROS2 driver
int throttlePercentEffortDriver   = 0;    // variable holds the current throttle effort commanded by the ROS2 driver
byte desiredGear = 1;                     // holds the "gear" desired by the ROS2 driver, 1, 2, or 3

double throttlePercentEffort  = 0;        // these are the global % effort variables used to smooth transitions between states
byte currentGear = 1;                     // holds the "gear" currently active 1, 2, or 3
byte brakeEngaged = false;

// ------Serial Variables------
byte messageStarted = false;
byte messageComplete = false;  // whether the string is complete
byte serialTimedOut = false;
byte receivedMessage[RX_PACKET_SIZE-2];

// ------Booleans------
// Probably should read an input from the wireless kill system
byte disablePressed = false;


//======================================================================================
//========================================Setup=========================================
//======================================================================================
void setup() 
{
  pinMode(DISABLE_PIN, INPUT);

  // Digital Output
  pinMode(GEAR_PIN_A, OUTPUT);
  pinMode(GEAR_PIN_B, OUTPUT);
  pinMode(BRAKE_PIN, OUTPUT);
  pinMode(REVERSE_PIN, OUTPUT);

  // Analog Output
  pinMode(THROTTLE_PIN, OUTPUT);
  
  //Initialize the serial
  Serial.begin(115200);
}


//======================================================================================
//=========================================Loop=========================================
//======================================================================================
void loop() 
{
  static unsigned long lastStateTime = 0, lastSendTime = 0, lastReceivedMsgTime = 0;
  static bool isValidMsg = false;
  unsigned long currentTime = 0;

  updateDI(); // Check for button presses

  if(messageComplete)
  { 
    isValidMsg = parseReceivedMessage(receivedMessage);
    if (isValidMsg){ lastReceivedMsgTime = millis(); }
  }
  
  currentTime = millis();
  if (currentTime >= (lastReceivedMsgTime + HEARTBEAT_TIMEOUT))
  {
    serialTimedOut = true;
  }

  if(currentTime >= (lastStateTime + STATE_TIMER))
  {
    stateLoop();
    lastStateTime = currentTime;
  }

  if(currentTime >= (lastSendTime + SEND_TIMER))
  {
    sendMessage();
    lastSendTime = currentTime;
  }
}
